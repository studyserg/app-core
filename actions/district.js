import API from '../utils/api';

export const GET_DISTRICT = 'GET_DISTRICT';

const _getDistrict = item => ({
    type: GET_DISTRICT,
    item
});

export function getDistrict() {
    return dispatch => {
        return API
            .districtGet()
            .then(item => dispatch(_getDistrict(item)));
    }
}
