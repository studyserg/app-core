export const NAVIGATE = 'NAVIGATE';
export const RESET = 'RESET';
export const BACK = 'BACK';

export const navigate = (route, params) => ({
    type: NAVIGATE,
    route,
    params
});

export const reset = (route, params) => ({
    type: RESET,
    route,
    params
});

export const back = () => ({
    type: BACK
});
