import API from '../utils/api';

export const REQUESTS_CATEGORY_LIST = 'REQUESTS_CATEGORY_LIST';
export const GET_CATEGORY_LIST = 'GET_CATEGORY_LIST';

const _requestsCategoryList = () => ({
    type: REQUESTS_CATEGORY_LIST
});

const _getCategoryList = items => ({
    type: GET_CATEGORY_LIST,
    items
});

export function getCategoryList() {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.categories.isFetching) {
            dispatch(_requestsCategoryList());

            return API
                .categoriesList()
                .then(items => dispatch(_getCategoryList(items)));
        }
    };
}
