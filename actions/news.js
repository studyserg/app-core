import API from '../utils/api';

export const REQUEST_GET_NEWS_LIST = 'REQUESTS_GET_NEWS_LIST';
export const GET_NEWS_LIST = 'GET_NEWS_LIST';
export const REQUEST_EXTEND_NEWS_LIST = 'REQUESTS_EXTEND_NEWS_LIST';
export const EXTEND_NEWS_LIST = 'EXTEND_NEWS_LIST';
export const REQUEST_REFRESH_NEWS_LIST = 'REQUEST_REFRESH_NEWS_LIST';
export const REFRESH_NEWS_LIST = 'REFRESH_NEWS_LIST';
export const RESET_NEWS = 'RESET_NEWS';

export const REQUESTS_CURRENT_NEWS = 'REQUEST_CURRENT_NEWS';
export const RECEIVE_CURRENT_NEWS = 'RECEIVE_CURRENT_NEWS';

export const REQUESTS_LIKE = 'REQUEST_ADD_LIKE';
export const RECEIVE_ADD_LIKE = 'RECEIVE_ADD_LIKE';
export const RECEIVE_REMOVE_LIKE = 'RECEIVE_REMOVE_LIKE';

const NEWS_LIMIT = 10;

const _requestGetNewsList = () => ({
    type: REQUEST_GET_NEWS_LIST
});

const _getNewsList = items => ({
    type: GET_NEWS_LIST,
    items
});

const _requestExtendNewsList = () => ({
    type: REQUEST_EXTEND_NEWS_LIST
});

const _extendNewsList = items => ({
    type: EXTEND_NEWS_LIST,
    items
});

const _requestRefreshNewsList = () => ({
    type: REQUEST_REFRESH_NEWS_LIST
});

const _refreshNewsList = (category, items) => ({
    type: REFRESH_NEWS_LIST,
    category,
    items
});

export const refreshNewsList = (category, sort) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isFetching) {
            dispatch(_requestRefreshNewsList());

            return API
                .newsList(20, 0, category, sort)
                .then(items => dispatch(_refreshNewsList(category, items)));
        }
    };
};

export const getNewsList = (category, sort) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isFetching) {
            dispatch(_requestGetNewsList());

            API
                .newsList(NEWS_LIMIT, 0, category, sort)
                .then(items => dispatch(_getNewsList(items)));
        }
    };
};

export const extendNewsList = (category, sort) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isAllFetched && !state.news.isFetching) {
            dispatch(_requestExtendNewsList());

            API
                .newsList(NEWS_LIMIT, NEWS_LIMIT * state.news.page, category, sort)
                .then(items => dispatch(_extendNewsList(items)));
        }
    };
};

const _requestsCurrentNews = () => ({
    type: REQUESTS_CURRENT_NEWS
});

const _receiveCurrentNews = current => ({
    type: RECEIVE_CURRENT_NEWS,
    current
});

export const fetchCurrentNews = (id) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isCurrentFetching) {
            dispatch(_requestsCurrentNews());

            return API
                .newsDetail(id)
                .then(current => dispatch(_receiveCurrentNews(current)));
        }
    };
};

const _requestsLike = () => ({
    type: REQUESTS_LIKE
});

const _receiveAddLike = (id, vkId) => ({
    type: RECEIVE_ADD_LIKE,
    id,
    vkId
});

export const addLike = (id) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isLikeRequest) {
            dispatch(_requestsLike());

            API
                .addLike(id, state.user.dump.id)
                .then(() => dispatch(_receiveAddLike(id, state.user.dump.id)));
        }
    };
};

const _receiveRemoveLike = (id, vkId) => {
    return {
        type: RECEIVE_REMOVE_LIKE,
        id,
        vkId
    }
};

export const removeLike = (id) => {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.news.isLikeRequest) {
            dispatch(_requestsLike());

            API
                .removeLike(id, state.user.dump.id)
                .then(() => dispatch(_receiveRemoveLike(id, state.user.dump.id)));
        }
    };
};

export const resetNews = () => {
    return dispatch => {
        dispatch({ type: RESET_NEWS });
    };
};