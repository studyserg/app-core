import { AsyncStorage } from 'react-native';

const STORAGE_PREFIX = '@DISTRICTS';
const DUMP_USER_STORAGE_KEY = `${STORAGE_PREFIX}:DUMP_USER`;

const _fetchUserData = data => ({
    type: FETCH_USER_DATA,
    data
});

export const FETCH_USER_DATA = 'FETCH_USER_DATA';
export const fetchUserData = () => {
    return dispatch => {
        AsyncStorage
            .getItem(DUMP_USER_STORAGE_KEY)
            .then(data => {
                data = JSON.parse(data);

                if (!data) {
                    data = {
                        id: Math.random().toString(36).substring(7) + `_${Date.now()}`,
                        name: ''
                    };

                    return AsyncStorage
                        .setItem(DUMP_USER_STORAGE_KEY, JSON.stringify(data))
                        .then(() => data);
                }

                return data;
            })
            .then(data => dispatch(_fetchUserData(data)));
    };
};

const _setUserName = name => ({
    type: SET_USER_NAME,
    name
});

export const SET_USER_NAME = 'SET_USER_NAME';
export const setUsername = (name) => {
    return (dispatch, getState) => {
        const state = getState();
        const data = { id: state.user.dump.id, name };

        AsyncStorage
            .setItem(DUMP_USER_STORAGE_KEY, JSON.stringify(data))
            .then(id => dispatch(_setUserName(data.name)));
    };
};