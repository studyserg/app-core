import API from '../utils/api';

export const REQUESTS_COMMENTS_LIST = 'REQUESTS_COMMENTS_LIST';
export const GET_COMMENTS_LIST = 'RECEIVE_COMMENTS_LIST';
export const ADD_COMMENT_ITEM = 'ADD_COMMENT_ITEM';

const COMMENT_LIMIT = 100;

const _requestsCommentsList = () => ({
    type: REQUESTS_COMMENTS_LIST
});

const _getCommentsList = items => ({
    type: GET_COMMENTS_LIST,
    items
});

const _addCommentItem = item => ({
    type: ADD_COMMENT_ITEM,
    item
});

export function getCommentList(newsId) {
    return (dispatch, getState) => {
        const state = getState();

        if (!state.comments.isFetching) {
            dispatch(_requestsCommentsList());

            API
                .commentList(newsId, COMMENT_LIMIT, COMMENT_LIMIT * getState().comments.page)
                .then(items => dispatch(_getCommentsList(items)));
        }
    };
}

export function addCommentItem(newsId, text) {
    return (dispatch, getState) => {
        API
            .commentSave(newsId, text, getState().user.dump.id, getState().user.dump.name)
            .then(item => dispatch(_addCommentItem(item)));
    };
}
