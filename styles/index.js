import { Platform } from 'react-native';

export default {
    constants: {
        APP_MAIN_COLOR: '#507299',

        HEADER_TEXT_COLOR: '#fff',
        HEADER_MARGIN: 10,

        FONT_REGULAR: Platform.OS === 'ios' ?
            'MuseoSansCyrl-300' :
            'MuseoSansRegular',
        FONT_BOLD: Platform.OS === 'ios' ?
            'MuseoSansCyrl-700' :
            'MuseoSansBold'
    },

    helpers: {
        centrify: {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        }
    }
};
