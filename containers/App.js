import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { BackHandler, Platform } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import DrawerLayout from 'react-native-drawer-layout';
import { addNavigationHelpers } from 'react-navigation/src/react-navigation';
import { fetchUserData } from '../actions/user';
import { getDistrict } from '../actions/district';
import { back } from '../actions/nav';
import AppNavigator from '../navigator';
import Menu from './Menu';

@connect(
    state => ({
        nav: state.nav
    }),
    dispatch => ({
        dispatch,
        back: () => dispatch(back()),
        fetchUserData: () => dispatch(fetchUserData()),
        getDistrict: () => dispatch(getDistrict())
    })
)
export default class AppWithNavigationState extends PureComponent {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        nav: PropTypes.object.isRequired
    };

    componentDidMount() {
        Promise
            .all([
                this.props.getDistrict(),
                this.props.fetchUserData()
            ])
            .then(() => SplashScreen.hide());

        if (Platform.OS === 'android') {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                if (this._drawer.state.drawerShown) {
                    this._drawer.closeDrawer();
                } else if (this.props.nav.index === 0) {
                    BackHandler.exitApp();
                } else {
                    this.props.back();
                }

                return true;
            });
        }
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') {
            this.backHandler.remove();
        }
    }

    render() {
        const { dispatch, nav } = this.props;
        const navigation = addNavigationHelpers({ dispatch, state: nav });

        return (
            <DrawerLayout
                drawerWidth={300}
                useNativeAnimations={true}
                ref={ref => this._drawer = ref}
                renderNavigationView={() => <Menu getDrawer={() => this.getDrawer()} />}
            >
                <AppNavigator navigation={navigation} screenProps={{ getDrawer: () => this.getDrawer() }} />
            </DrawerLayout>
        );
    }

    getDrawer() {
        return this._drawer;
    }
}
