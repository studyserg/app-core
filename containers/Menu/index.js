import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
    Image,
    ScrollView,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import { getCategoryList } from '../../actions/categories';
import { navigate, reset } from '../../actions/nav';
import { resetNews } from '../../actions/news';
import Text from '../../components/Text';
import { Touchable } from '../../utils/components';
import styles from './styles';

const BACKGROUND_HEADER = require('./images/menu__header.png');
const MAIN_CATEGORIES = [
    'Вакансии',
    'Акции'
];

@connect(
    state => ({
        categories: state.categories.data
    }),
    dispatch => ({
        getCategoryList: () => dispatch(getCategoryList()),
        navigate: (route, params) => dispatch(navigate(route, params)),
        reset: (route, params) => dispatch(reset(route, params)),
        resetNews: () => dispatch(resetNews())
    })
)
export default class Menu extends PureComponent {
    state = {
        selected: 'Home'
    };

    constructor(props) {
        super(props);

        this.list = [];
        this.mainCategories = [];
        this.additionalCategories = [];
    }

    componentDidMount() {
        this.props.getCategoryList();
    }

    render() {
        if (!this.prepared) this.prepareMenu(this.props.categories);

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.header__image} source={BACKGROUND_HEADER} />
                </View>

                <ScrollView style={styles.body}>
                    <Touchable onPress={() => this.navigate('Home', 'Home', {}, true)}>
                        <View style={[styles.submenu__item, styles.centrify, this.isActive('Home') ? styles.submenu__item_active : null ]}>
                            <View style={styles.centrify}>
                                <FontAwesomeIcon
                                    style={[styles.submenu__icon, this.isActive('Home') ? styles.submenu__icon_active : null]}
                                    size={25}
                                    name='newspaper-o'
                                />
                                <Text style={[styles.submenu__name]}>Лента</Text>
                            </View>
                        </View>
                    </Touchable>

                    {
                        this.mainCategories.map(item => {
                            const isActive = this.isActive(`Category${item._id}`);
                            const icon = item.name === 'Вакансии' ?
                                'briefcase' :
                                'basket-loaded';

                            return (
                                <Touchable key={item._id} onPress={() => this.navigate(`Category${item._id}`, 'Category', item, true)}>
                                    <View style={[styles.submenu__item, styles.centrify, isActive ? styles.submenu__item_active : null]}>
                                        <View style={styles.centrify}>
                                            <SimpleLineIcon
                                                style={[{ paddingLeft: 1 }, styles.submenu__icon, isActive ? styles.submenu__icon_active : null]}
                                                size={25}
                                                name={icon}
                                            />
                                            <Text style={[styles.submenu__name, isActive ? styles.submenu__name_active : null]}>{item.name}</Text>
                                        </View>
                                    </View>
                                </Touchable>
                            );
                        })
                    }

                    <View style={styles.divider} />

                    <View style={[styles.tags]}>
                        {
                            this.additionalCategories.map(item => {
                                const isActive = this.isActive(`Category${item._id}`);

                                return (
                                    <Touchable key={item._id} onPress={() => this.navigate(`Category${item._id}`, 'Category', item, true)}>
                                        <View style={styles.tags__item}>
                                            <Text style={[styles.tags__text, isActive ? styles.tags__text_active : null]}>
                                                #{item.name}
                                            </Text>
                                        </View>
                                    </Touchable>
                                );
                            })
                        }
                    </View>
                    <View style={styles.divider } />
                </ScrollView>

                <View style={styles.footer}>
                    <Touchable onPress={() => this.navigate('Popular', 'Popular', {}, true)}>
                        <View style={[styles.submenu__item, styles.centrify, this.isActive('Popular') ? styles.submenu__item_active : null]}>
                            <View style={styles.centrify}>
                                <FontAwesomeIcon
                                    style={styles.submenu__icon}
                                    size={25}
                                    name='star-o'
                                />
                                <Text style={styles.submenu__name}>
                                    Обсуждаемое
                                </Text>
                            </View>
                        </View>
                    </Touchable>

                    <Touchable onPress={() => this.navigate('Feedback', 'Feedback', {})}>
                        <View style={[styles.submenu__item, styles.centrify]}>
                            <View style={styles.centrify}>
                                <Icon
                                    style={[styles.submenu__icon, styles.submenu__icon_type_feedback]}
                                    size={25}
                                    name='md-document'
                                />
                                <Text style={styles.submenu__name}>
                                    Написать нам
                                </Text>
                            </View>
                        </View>
                    </Touchable>
                </View>
            </View>
        );
    }

    prepareMenu(categories) {
        if (!Object.keys(categories).length) return;

        this.list = Object
            .keys(categories)
            .map(id => categories[id]);
        this.mainCategories = this.list
            .filter(item => MAIN_CATEGORIES.includes(item.name));
        this.additionalCategories = this.list
            .filter(item => !MAIN_CATEGORIES.includes(item.name));
        this.prepared = true;
    }

    navigate(id, screen, params, replace = false) {
        if (replace) {
            this.setState({ selected: id });
            this.props.resetNews();
            this.props.reset(screen, params);
        } else {
            this.props.navigate(screen, params);
        }

        this.props.getDrawer().closeDrawer();
    }

    isActive(id) {
        return this.state.selected === id;
    }
}
