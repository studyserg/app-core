import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: '#fff'
    },

    header: {
        elevation: 10
    },

    header__image: {
        width: 300,
        height: 120,

        resizeMode: 'stretch'
    },

    submenu: {
        marginTop: 10
    },

    submenu__item: {
        paddingVertical: 7,
        paddingHorizontal: 15
    },

    submenu__icon_type_feedback: {
        paddingLeft: 5
    },

    submenu__item_active: {
        backgroundColor: '#e4e4e4'
    },

    submenu__icon: {
        color: '#434343',

        width: 30,
        marginRight: 20
    },

    submenu__icon_active: {
        color: '#f1a38d'
    },

    submenu__name: {
        fontFamily: globalStyles.constants.FONT_BOLD,
        fontSize: 15,

        color: '#222'
    },

    submenu__name_active: {
        color: '#f1a38d'
    },

    submenu__unreaded: {
        paddingVertical: 2,
        paddingHorizontal: 8,

        borderRadius: 7,

        backgroundColor: '#e4e4e4'
    },

    submenu__count: {
        fontFamily: globalStyles.constants.FONT_BOLD,

        color: '#f1a38d'
    },

    tags: {
        flex: 1,
        flexDirection:'row',
        flexWrap:'wrap',

        marginTop: 10,
        paddingHorizontal: 15
    },

    tags__item: {
        marginBottom: 8,
        marginRight: 8,
        paddingVertical: 3,
        paddingHorizontal: 8,

        borderRadius: 7,

        backgroundColor: '#d8d9da'
    },

    tags__text: {
        fontSize: 15,
        color: '#000'
    },

    tags__text_active: {
        color: '#537495'
    },

    divider: {
        marginVertical: 5,

        borderBottomColor: 'rgba(0, 0, 0, 0.1)',
        borderBottomWidth: 1
    },

    body: {
        marginTop: 10,
        marginBottom: 90
    },

    footer: {
        position: 'absolute',
        right: 0,
        bottom: 0,
        left: 0,


        paddingBottom: 15,
        elevation: 10
    },

    centrify: globalStyles.helpers.centrify
});
