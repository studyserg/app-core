import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    icon: {
        color: globalStyles.constants.HEADER_TEXT_COLOR,

        paddingVertical: 10,
        paddingHorizontal: globalStyles.constants.HEADER_MARGIN
    }
});
