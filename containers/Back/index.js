import React, { Component } from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { back } from '../../actions/nav';
import styles from './styles';

@connect(
    null,
    dispatch => ({
        back: () => dispatch(back())
    })
)
export default class Back extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <Icon
                style={styles.icon}
                name='md-arrow-back'
                size={25}
                onPress={this.props.back}
            />
        );
    }
}
