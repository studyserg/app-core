import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';

const enhancer = compose(
    applyMiddleware(thunk)
);

export default function configureStore(initialState) {
    const store = createStore(reducer, initialState, enhancer);

    if (module.hot) {
        module.hot.accept(() => {
            store.replaceReducer(require('./reducers').default);
        });
    }

    return store;
}
