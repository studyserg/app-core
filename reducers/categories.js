import {
    REQUESTS_CATEGORY_LIST,
    GET_CATEGORY_LIST
} from '../actions/categories';

const INITIAL_STATE = {
    data: {},
    isFetching: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case REQUESTS_CATEGORY_LIST:
            return Object.assign({}, state, { isFetching: true });
        case GET_CATEGORY_LIST:
            return Object.assign({}, state, {
                isFetching: false,
                data: action.items.reduce((result, item) => {
                    result[item._id] = item;
                    return result;
                }, {})
            });
        default:
            return state;
    }
};
