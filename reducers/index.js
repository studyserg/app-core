import { combineReducers } from 'redux';
import nav from './nav';
import news from './news';
import categories from './categories';
import comments from './comments';
import user from './user';
import district from './district';

export default combineReducers({
    nav,
    news,
    categories,
    comments,
    user,
    district
});
