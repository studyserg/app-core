import { FETCH_USER_DATA, SET_USER_NAME } from '../actions/user';

const INITIAL_STATE = {
    dump: {
        id: null,
        name: ''
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_USER_DATA:
            return Object.assign({}, state, { dump: { id: action.data.id, name: action.data.name } });
        case SET_USER_NAME:
            const dump = Object.assign(state.dump, { name: action.name });

            return Object.assign({}, state, { dump: dump });
        default:
            return state;
    }
};
