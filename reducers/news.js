import {
    REQUEST_GET_NEWS_LIST,
    GET_NEWS_LIST,
    REQUEST_EXTEND_NEWS_LIST,
    EXTEND_NEWS_LIST,
    REQUEST_REFRESH_NEWS_LIST,
    REFRESH_NEWS_LIST,
    RESET_NEWS,

    REQUESTS_CURRENT_NEWS,
    RECEIVE_CURRENT_NEWS,

    REQUESTS_LIKE,
    RECEIVE_ADD_LIKE,
    RECEIVE_REMOVE_LIKE

} from '../actions/news';

const INITIAL_STATE = {
    current: null,
    page: 0,
    items: [],
    isFetching: false,
    isCurrentFetching: false,
    isLikeRequest: false,
    isAllFetched: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case RESET_NEWS:
            return Object.assign({}, state, {
                isFetching: false,
                page: 0,
                items: [],
                isAllFetched: false
            });

        case REQUEST_GET_NEWS_LIST:
            return Object.assign({}, state, {
                isFetching: true,
                page: 0,
                items: [],
                isAllFetched: false
            });
        case GET_NEWS_LIST:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.items,
                page: state.page + 1,
                isAllFetched: action.items.length < 10
            });
        case REQUEST_EXTEND_NEWS_LIST:
            return Object.assign({}, state, {
                isFetching: true
            });
        case EXTEND_NEWS_LIST:
            return Object.assign({}, state, {
                isFetching: false,
                items: state.items.concat(action.items),
                page: state.page + 1,
                isAllFetched: action.items.length < 10
            });

        case REQUEST_REFRESH_NEWS_LIST:
            return Object.assign({}, state, { isFetching: true });
        case REFRESH_NEWS_LIST:
            const refreshed = action.items.filter(item => {
                if (action.category && !item.categories.includes(action.category)) return false;
                return item.createdAt > state.items[0].createdAt;
            });
            let items = state.items;
            let page = state.page;

            if (refreshed.length) {
                items = refreshed.concat(state.items);
                page = Math.floor(items.length / 10);
            }

            return Object.assign({}, state, {
                isFetching: false,
                items,
                page
            });

        case REQUESTS_CURRENT_NEWS:
            return Object.assign({}, state, {
                isCurrentFetching: true,
                current: null
            });
        case RECEIVE_CURRENT_NEWS:
            return Object.assign({}, state, {
                isCurrentFetching: false,
                items: state.items.map(item => {
                    return item._id === action.current._id ?
                        Object.assign({}, item, action.current) :
                        item;
                }),
                current: action.current
            });

        case REQUESTS_LIKE:
            return Object.assign({}, state, { isLikeRequest: true });
        case RECEIVE_ADD_LIKE:
            return Object.assign({}, state, {
                isLikeRequest: false,
                items: state.items.map(item => {
                    return item._id === action.id ?
                        Object.assign({}, item, { likes: [].concat(item.likes, [action.vkId]) }) :
                        item;
                }),
                current: state.current ?
                    Object.assign({}, state.current, {
                        likes: [].concat(state.current.likes, [action.vkId])
                    }) :
                    null
            });
        case RECEIVE_REMOVE_LIKE:
            return Object.assign({}, state, {
                isLikeRequest: false,
                items: state.items.map(item => {
                    return item._id === action.id ?
                        Object.assign({}, item, { likes: item.likes.filter(id => id !== action.vkId) }) :
                        item;
                }),
                current: state.current ?
                    Object.assign({}, state.current, {
                        likes: state.current.likes.filter(id => id !== action.vkId)
                    }) :
                    null
            });
        default:
            return state;
    }
};
