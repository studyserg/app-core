import {
    GET_DISTRICT
} from '../actions/district';

const INITIAL_STATE = {
    data: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_DISTRICT:
            return Object.assign({}, state, { data: action.item });
        default:
            return state;
    }
};
