import {
    REQUESTS_COMMENTS_LIST,
    GET_COMMENTS_LIST,
    ADD_COMMENT_ITEM
} from '../actions/comments';

const INITIAL_STATE = {
    items: [],
    page: 0,
    isFetching: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case REQUESTS_COMMENTS_LIST:
            return Object.assign({}, state, { isFetching: true });
        case GET_COMMENTS_LIST:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.items
            });
        case ADD_COMMENT_ITEM:
            return Object.assign({}, state, {
                items: [action.item].concat(state.items)
            });
        default:
            return state;
    }
};
