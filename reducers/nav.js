import AppNavigator from '../navigator';
import { NavigationActions } from 'react-navigation';
import { NAVIGATE, RESET, BACK } from '../actions/nav';

const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Home'));

export default (state = initialState, action) => {
    switch(action.type) {
        case NAVIGATE:
            return AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: action.route, params: action.params }),
                state
            );
        case RESET:
            return AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index: 0,
                    actions: [
                        NavigationActions.navigate({ routeName: action.route, params: action.params})
                    ]
                }),
                state
            );
        case BACK:
            return AppNavigator.router.getStateForAction(
                NavigationActions.back(),
                state
            );
        default:
            return state;
    }
};
