import { StyleSheet } from 'react-native';

const PARALLAX_HEADER_HEIGHT = 350;

export default StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: '#eee'
    },

    shadow: {
        backgroundColor: 'rgba(0, 0, 0, .4)',
        height: PARALLAX_HEADER_HEIGHT,
        flex: 1
    },

    backgroundImage: {
        position: 'absolute',
        backgroundColor: 'transparent',
        overflow: 'hidden',
        top: 0,
        height: PARALLAX_HEADER_HEIGHT,
        width: '100%'
    },

    list: {
        marginHorizontal: 10
    }
});
