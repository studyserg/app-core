import React, { PureComponent } from 'react';
import {
    Animated,
    FlatList,
    ImageBackground,
    RefreshControl,
    View
} from 'react-native';
import AddNewsButton from '../AddNewsButton';
import NewsPreview from '../NewsPreview';
import ParallaxScrollViewForeground from '../ParallaxScrollViewForeground';
import styles from './styles';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const AnimatedBackgroundImage = Animated.createAnimatedComponent(ImageBackground);

export default class ParallaxScrollView extends PureComponent {
    state = {
        scrollY: new Animated.Value(0)
    };

    render() {
        return (
            <View
                style={styles.container}
                renderToHardwareTextureAndroid={true}
                shouldRasterizeIOS={true}
            >
                <AnimatedBackgroundImage
                    style={[
                        styles.backgroundImage,
                        {
                            transform: [{
                                translateY: this.state.scrollY.interpolate({
                                    inputRange: [0, 650],
                                    outputRange: [0, -350],
                                    extrapolate: 'clamp'
                                })
                            }]
                        }
                    ]}
                    source={{
                        uri: this.props.district.images && this.props.district.images.length && this.props.district.images[0].src,
                    }}
                >
                    <View style={styles.shadow} />
                </AnimatedBackgroundImage>

                <AnimatedFlatList
                    style={styles.list}
                    data={this.props.news}
                    keyExtractor={item => item._id}
                    renderItem={info => <NewsPreview
                        {...info.item}
                        categoriesData={this.props.categoriesData}
                        user={this.props.user}
                        navigate={this.props.navigate}
                        addLike={this.props.addLike}
                        removeLike={this.props.removeLike}
                    />}

                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.refreshing}
                            onRefresh={this.props.onRefresh.bind(this)}
                        />
                    }

                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={10}
                    onScroll={
                        Animated.event(
                            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
                            { useNativeDriver: true }
                        )
                    }

                    onEndReachedThreshold={500}
                    initialNumToRender={10}
                    maxToRenderPerBatch={10}
                    renderToHardwareTextureAndroid={true}
                    removeClippedSubviews={true}
                    onEndReached={() => this.props.loadMore()}
                    enableEmptySections={true}
                    ListHeaderComponent={<ParallaxScrollViewForeground name={this.props.district.name} />}
                />

                <AddNewsButton
                    addNews={() => this._messageNavigate('Message')}
                    addVacancy={() => this._messageNavigate('VacancyMessage')}
                />
            </View>
        );
    }

    _messageNavigate(route) {
        this.props.navigate(route);
    }
}
