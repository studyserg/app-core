import React, { PureComponent } from 'react';
import { Text } from 'react-native';
import globalStyles from '../../styles';

export default class AppText extends PureComponent {
    render() {
        return (
            <Text {...this.props} style={[{ fontFamily: globalStyles.constants.FONT_REGULAR }, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}
