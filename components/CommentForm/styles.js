import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        margin: 10,
        borderWidth: 1,
        borderColor: '#9b9b9b'
    },

    bottom: {
        paddingVertical: 20,
        paddingHorizontal: 10,
        backgroundColor: '#fffdf1'
    },

    input__text: {
        fontSize: 13,
        height: 100,
        textAlignVertical: 'top',
        marginHorizontal: 10
    },

    input__name: {
        fontSize: 13,
        paddingVertical: 0,
        paddingHorizontal: 5,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#9b9b9b',
        marginBottom: 10
    },

    divider: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.1)'
    },

    bottom__info: {
        fontSize: 11,
        width: 90
    },

    bottom__error: {
        flex: 1,
        fontSize: 11,

        marginBottom: 15,

        color: '#f00'
    },

    bottom__save: {
        fontFamily: globalStyles.constants.FONT_BOLD,
        color: globalStyles.constants.APP_MAIN_COLOR
    }
});
