import React, { PureComponent } from 'react';
import { Keyboard, TextInput, TouchableWithoutFeedback, View } from 'react-native';
import Text from '../../components/Text';
import globalStyles from '../../styles';
import styles from './styles';

export default class CommentForm extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            name: props.name,
            text: '',
            submitted: false
        };
    }

    render() {
        return (
            <View style={styles.container} onLayout={this.props.onLayout}>
                <TextInput
                    style={styles.input__text}
                    placeholderTextColor='#999'
                    placeholder='Оставьте свой комментарий ...'
                    underlineColorAndroid='transparent'
                    multiline={true}
                    numberOfLines={4}
                    value={this.state.text}
                    onChangeText={value => this.setState({ text: value })}
                />

                <View style={styles.divider} />

                <View style={styles.bottom}>
                    <TextInput
                        style={styles.input__name}
                        placeholderTextColor='#999'
                        placeholder='Укажите ваше имя'
                        underlineColorAndroid='transparent'
                        value={this.state.name}
                        onChangeText={value => this.setState({ name: value })}
                        onBlur={() => this.updateName()}
                    />
                    {
                        this.state.submitted && !this.state.name ?
                            <Text style={styles.bottom__error}>
                                Вы забыли указать имя
                            </Text> :
                            null
                    }
                    <View style={[globalStyles.helpers.centrify]}>
                        <Text style={styles.bottom__info}>
                            Комментарии проходят премодерацию
                        </Text>
                        <TouchableWithoutFeedback onPress={() => this.save()}>
                            <Text style={styles.bottom__save}>
                                Ответить
                            </Text>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>
        );
    }

    save() {
        Keyboard.dismiss();
        this.setState({ submitted: true });

        if (!this.state.text || !this.state.name) return;

        this.props.saveComment(this.state.text);
        this.setState({ text: '' });
    }

    updateName() {
        if (!this.state.name) return;

        this.props.saveUsername(this.state.name)
    }
};
