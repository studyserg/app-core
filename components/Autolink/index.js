import React, { PureComponent, createElement } from 'react';
import { Text, Linking } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const PHONE_REGEXP = /[^(__phone__|__email__|__url__)]((\+7|7|8)?([0-9]){10})/gi;
const EMAIL_REGEXP = /[^(__phone__|__email__|__url__)](([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})/gi;
const URL_REGEXP =   /[^(__phone__|__email__|__url__)]https?:\/\/([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/gi;

export default class Autolink extends PureComponent {
    static propTypes = {
        text: PropTypes.string
    };

    render() {
        const text = this.props.text
            .replace(PHONE_REGEXP, origin => {
                return `__phone__${origin}__phone__`;
            })
            .replace(EMAIL_REGEXP, origin => {
                return `__email__${origin}__email__`;
            })
            .replace(URL_REGEXP, origin => {
                return `__url__${origin}__url__`;
            });
        const nodes = text.split(/(__phone__|__email__|__url__)/gi);
        const result = [];

        for (let i = 0; i < nodes.length; ) {
            const node = nodes[i];

            if (node) {
                switch(node) {
                    case '__phone__':
                    case '__email__':
                    case '__url__':
                        let value = nodes[i + 1];

                        result.push(
                            <Text
                                key={result.length}
                                style={styles.link}
                                onPress={() => this.callAction(node, value)}
                            >
                                {value}
                            </Text>
                        );
                        i += 3;
                        break;
                    default:
                        result.push(<Text key={result.length}>{node}</Text>);
                        i++;
                }
            } else {
                i++;
            }
        }

        return createElement(Text, this.props, result);
    }

    callAction(action, value) {
        let link;

        value = value.trim();

        if (action === '__phone__') {
            if (value.slice(0, 2) !== '+7' && value[0] !== '8') {
                value = `+7${value}`;
            }

            link = `tel:${value}`;
        } else if (action === '__email__') {
            link = `mailto:${value}`;
        } else if (action === '__url__') {
            link = `${value}`;
        }

        Linking.openURL(link);
    }
}
