import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    link: {
        color: globalStyles.constants.APP_MAIN_COLOR
    }
});

