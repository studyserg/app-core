import React, { PureComponent } from 'react';
import {
    Dimensions,
    Image,
    Linking,
    ScrollView,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import Autolink from '../Autolink';
import CommentList from '../CommentList';
import CommentForm from '../CommentForm';
import Like from '../Like';
import Slider from '../Slider';
import Text from '../Text';
import dateUtils from '../../utils/date';
import textUtils from '../../utils/text';
import globalStyles from '../../styles';
import styles from './styles';

export default class News extends PureComponent {
    state = {
        isScrolled: false
    };

    constructor(props) {
        super(props);

        this._onCommentLayout = this._onCommentLayout.bind(this);
    }

    render() {
        return (
            <View style={styles.outer}>
                <ScrollView
                    style={styles.container}
                    ref={ref => this._scroll = ref}
                    keyboardShouldPersistTaps='handled'
                >
                    {
                        this.props.title ?
                            <Text style={[styles.wrapper, styles.title]} selectable={true}>
                                {textUtils.capitalize(this.props.title)}
                            </Text> :
                            null
                    }
                    <View style={[styles.wrapper, globalStyles.helpers.centrify, styles.info]}>
                        {
                            this.props.categories.length ?
                                this.props.categories.map(id =>
                                    <Text key={id} style={styles.category}>
                                        {this.props.categoriesData[id] && this.props.categoriesData[id].name}
                                    </Text>
                                ) :
                                null
                        }
                        <Text style={styles.time}>{dateUtils.getDate(this.props.createdAt)}</Text>
                    </View>
                    <Autolink
                        style={[styles.wrapper, styles.text]}
                        selectable={true}
                        text={this.props.text}
                    />
                    {
                        this.props.slider[0] && this.props.slider[0].height / this.props.slider[0].width > 1 ?
                            <Image
                                style={{ flex: 1 }}
                                height={Dimensions.get('window').width * (this.props.slider[0].height / this.props.slider[0].width)}
                                source={{ uri: this.props.slider[0].src }}
                            /> :
                            <Slider
                                style={styles.slider}
                                height={220}
                                items={this.props.slider}
                            />
                    }
                    <View style={[
                        styles.wrapper,
                        this.props.sourceUrl ? globalStyles.helpers.centrify : { flexDirection: 'row-reverse' },
                        styles.controls
                    ]}>
                        {
                            this.props.sourceUrl ?
                                <TouchableWithoutFeedback onPress={() => this.onSourceClick()}>
                                    <Text style={styles.source__text}>Ссылка на источник</Text>
                                </TouchableWithoutFeedback> :
                                null
                        }
                        <Like
                            addLike={() => this.props.addLike(this.props._id)}
                            removeLike={() => this.props.removeLike(this.props._id)}
                            isVoted={this.props.user.dump && this.props.likes.includes(this.props.user.dump.id)}
                            count={this.props.likes.length}
                        />
                    </View>

                    <View style={styles.divider} />
                    <CommentForm
                        onLayout={this._onCommentLayout}
                        name={this.props.user.dump.name}
                        saveUsername={name => this.saveUsername(name)}
                        saveComment={text => this.saveComment(text)}
                    />
                    <View style={styles.divider} />
                    <CommentList items={this.filterComments()} />
                </ScrollView>
            </View>
        );
    }

    filterComments() {
        return this.props.comments
            .filter(item => item.isPublish || item.authorId === this.props.user.dump.id)
    }

    saveComment(text) {
        this.props.addCommentItem(this.props._id, text);
    }

    saveUsername(name) {
        this.props.setUsername(name);
    }

    onSourceClick() {
        Linking.openURL(this.props.sourceUrl);
    }

    _onCommentLayout(event) {
        if (this.props.isScrollToComment && !this.state.isScrolled) {
            this.setState({ isScrolled: true });
            this._scroll.scrollTo({ x: 0, y: event.nativeEvent.layout.y });
        }
    }
}
