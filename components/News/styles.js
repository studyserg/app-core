import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    outer: {
        flex: 1
    },

    container: {
        paddingVertical: 10,

        backgroundColor: '#fff'
    },

    wrapper: {
        paddingHorizontal: 10
    },

    title: {
        fontSize: 30,
        fontWeight: '700',

        color: '#222',

        marginBottom: 5
    },

    info: {
        justifyContent: 'flex-start'
    },

    category: {
        fontSize: 16,

        color: globalStyles.constants.APP_MAIN_COLOR,
        fontWeight: '700',
        marginRight: 10
    },

    time: {
        fontSize: 14,
        lineHeight: 18,
        color: '#999'
    },

    slider: {
        marginTop: 10,
        marginBottom: 10
    },

    text: {
        fontSize: 16,
        fontFamily: globalStyles.constants.FONT_REGULAR,

        color: '#222',

        marginTop: 10
    },

    likeIcon: {
        paddingRight: 5
    },

    controls: {
        marginTop: 10,
        marginBottom: 15
    },

    divider: {
        paddingVertical: 10,
        backgroundColor: '#eee'
    },

    source: {
        flexDirection: 'row-reverse'
    },

    source__text: {
        fontFamily: globalStyles.constants.FONT_BOLD,
        fontSize: 11,

        color: globalStyles.constants.APP_MAIN_COLOR
    }
});
