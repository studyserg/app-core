import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        marginBottom: 15
    },

    text: {
        color: '#222',

        marginBottom: 10
    },

    info: {
        flexDirection: 'row-reverse'
    },

    author: {
        fontSize: 11,
        fontWeight: '700',
        color: globalStyles.constants.APP_MAIN_COLOR
    },

    date: {
        fontSize: 11,
        fontWeight: '700',
        color: '#999'
    },

    divider: {
        paddingTop: 15,

        borderBottomWidth: 1,
        borderBottomColor: 'rgba(0, 0, 0, 0.1)'
    }
});
