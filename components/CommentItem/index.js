import React, { PureComponent } from 'react';
import { View } from 'react-native';
import Text from '../Text';
import dateUtils from '../../utils/date';
import globalStyles from '../../styles';
import styles from './styles';

export default class CommentItem extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.text}</Text>
                <View style={this.props.authorName ? globalStyles.helpers.centrify : styles.info}>
                    <Text style={styles.author}>
                        {this.props.authorName}
                    </Text>
                    <Text style={styles.date}>
                        {dateUtils.getDate(this.props.createdAt)}
                    </Text>
                </View>
                <View style={styles.divider} />
            </View>
        );
    }
};
