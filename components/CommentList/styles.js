import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        marginBottom: 30
    },

    title: {
        fontFamily: globalStyles.constants.FONT_REGULAR,
        color: '#222',

        marginTop: 10,
        marginBottom: 20,

        textAlign: 'center'
    }
});
