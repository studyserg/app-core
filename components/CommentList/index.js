import React, { PureComponent } from 'react';
import { Text, View } from 'react-native';
import CommentItem from '../CommentItem';
import textUtils from '../../utils/text';
import styles from './styles';

export default class CommentList extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>
                    {
                        this.props.items.length ?
                            this.props.items.length + ' ' + textUtils.plural(
                            this.props.items.length,
                            'КОММЕТАРИЙ',
                            'КОММЕТАРИЯ',
                            'КОММЕТАРИЕВ',
                            ) :
                            'Ещё нет комментариев'
                    }
                </Text>
                { this.props.items.map(item => <CommentItem key={item._id} {...item} />) }
            </View>
        );
    }
};
