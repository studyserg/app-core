import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

export default class Navicon extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <Icon
                style={styles.icon}
                name='md-menu'
                size={30}
                onPress={() => this.props.getDrawer().openDrawer()}
            />
        );
    }
}
