import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import globalStyles from '../../styles';
import Text from '../Text';
import styles from './styles';

export default class Like extends PureComponent {
    static propTypes = {
        isVoted: PropTypes.bool.isRequired,
        count: PropTypes.number.isRequired,
        addLike: PropTypes.func.isRequired,
        removeLike: PropTypes.func.isRequired
    };

    render() {
        return (
            <View style={[styles.centrify, styles.container]}>
                <Icon
                    style={styles.icon}
                    name='md-thumbs-up'
                    size={20}
                    color={this.props.isVoted ?globalStyles.constants.APP_MAIN_COLOR : '#999'}
                    onPress={() => this.props.isVoted ? this.props.removeLike() : this.props.addLike()}
                />
                {
                    this.props.count ?
                        <Text style={styles.count}>{this.props.count}</Text> :
                        null
                }
            </View>
        );
    }
}
