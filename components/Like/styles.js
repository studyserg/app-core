import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    centrify: globalStyles.helpers.centrify,

    container: {
        justifyContent: 'flex-start',
        alignItems: 'center'
    },

    icon: {
        padding: 8
    },

    count: {
        paddingTop: 3,
        fontSize: 11
    }
});
