import React, { Component } from 'react';
import {
    TouchableOpacity,
    View
} from 'react-native';
import DialogAndroid from 'react-native-dialogs';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

export default class AddNewsButton extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => {
                    const dialog = new DialogAndroid();

                    dialog.set({
                        title: 'Выберите действие',
                        items: [
                            'Добавить новость',
                            'Добавить вакансию'
                        ],
                        itemsCallbackSingleChoice: id => {
                            switch(id) {
                                case 0:
                                    this.props.addNews();
                                    return;
                                case 1:
                                    this.props.addVacancy();
                                    return;
                            }
                        }
                    });
                    dialog.show();
                }}>
                    <View style={styles.button}>
                        <Icon
                            style={styles.icon}
                            name='md-add'
                            color='#fff'
                        />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
