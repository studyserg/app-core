import React, { Component } from 'react';
import { View } from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './styles';

export default class AddNewsButton extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        return (
            <View style={styles.container}>
                <ModalSelector
                    ref={ref => this._modal = ref}
                    animationType='fade'
                    data={[
                        { key: 0, label: 'Добавить новость' },
                        { key: 1, label: 'Добавить вакансию' }
                    ]}
                    cancelStyle={{ display: 'none' }}
                    onChange={option => {
                        this._modal.close();
                        switch(option.key) {
                            case 0:
                                this.props.addNews();
                                break;
                            case 1:
                                this.props.addVacancy();
                                break;
                        }
                    }}
                >
                    <View style={styles.button}>
                        <Icon
                            style={styles.icon}
                            name='md-add'
                            color='#fff'
                        />
                    </View>
                </ModalSelector>
            </View>
        );
    }
}
