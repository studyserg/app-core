import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        position: 'absolute',
        elevation: 2,

        bottom: 20,
        right: 20,

        backgroundColor: globalStyles.constants.APP_MAIN_COLOR,

        width: 60,
        height: 60,

        borderRadius: 30
    },

    button: {
        width: 60,
        height: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },

    icon: {
        fontSize: 20,
        height: 22
    }
});
