import React, { PureComponent } from 'react';
import { Animated, View, Text } from 'react-native';
import styles from './styles';

export default class ParallaxScrollViewForeground extends PureComponent {
    render() {
        return (
            <View style={styles.container} removeClippedSubviews={true}>
                <Animated.View style={styles.animated}>
                    <View style={styles.header}>
                        <Text style={styles.text}>
                            {this.props.name}
                        </Text>
                    </View>
                </Animated.View>
            </View>
        );
    }
}
