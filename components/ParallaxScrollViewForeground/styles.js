import { StyleSheet } from 'react-native';

const PARALLAX_HEADER_HEIGHT = 350;

export default StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        overflow: 'hidden'
    },

    animated: {
        height: PARALLAX_HEADER_HEIGHT,
        backgroundColor: 'transparent',
        overflow: 'hidden',
    },

    header: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',

        paddingTop: 100
    },

    text: {
        fontSize: 24,
        color: '#fff',

        marginTop: 120,
        paddingVertical: 5
    }
});
