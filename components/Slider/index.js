import React, { PureComponent } from 'react';
import { Image, View } from 'react-native';
import Swiper from 'react-native-swiper';
import Text from '../Text';

const TITLE_HEIGHT = 30;

export default class Slider extends PureComponent {
    render() {
        if (!this.props.items.length) return null;

        return (
            <View style={[this.props.style]}>
                <Swiper
                    containerStyle={{ height: this.props.height + TITLE_HEIGHT }}
                    buttonWrapperStyle={this.props.buttonWrapperStyle}
                    removeClippedSubviews={true}
                    width={this.props.width}
                    height={this.props.height}
                    loop={false}
                    showsPagination={false}
                    showsButtons={true}
                    loadMinimal={true}
                    loadMinimalSize={2}
                >
                    {
                        this.props.items.map((image, index) => <Image
                            key={index}
                            title={
                                this.props.items.length > 1 ?
                                    <Text>{`${index + 1} из ${this.props.items.length}`}</Text> :
                                    null
                            }
                            style={{ width: this.props.width, height: this.props.height }}
                            resizeMode={'contain'}
                            source={{ uri: image.src }}
                        />)
                    }
                </Swiper>
            </View>
        );
    }
}
