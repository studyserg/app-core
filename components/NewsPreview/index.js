import React, { PureComponent } from 'react';
import {
    Dimensions,
    Image,
    TouchableWithoutFeedback,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import dateUtils from '../../utils/date';
import textUtils from '../../utils/text';
import globalStyles from '../../styles';
import Autolink from '../Autolink';
import Like from '../Like';
import Slider from '../Slider';
import Text from '../Text';
import styles from './styles';

const TEXT_PREVIEW_LENGTH = 100;

export default class NewsPreview extends PureComponent {
    render() {
        let isLongText = false;
        let text = this.props.text || '';

        if (text.length > TEXT_PREVIEW_LENGTH) {
            isLongText = true;
            text = text.slice(0, TEXT_PREVIEW_LENGTH) + ' ...';
        }

        return (
            <View style={styles.container}>
                <TouchableWithoutFeedback
                    onPress={() => this.navigate()}
                    onLongPress={() => false}
                >
                    <View style={styles.wrapper}>
                        <View style={[globalStyles.helpers.centrify, styles.info]}>
                            {
                                this.props.categories.length ?
                                    this.props.categories.map(id =>
                                        <Text key={id} style={styles.category}>
                                            {this.props.categoriesData[id] && this.props.categoriesData[id].name}
                                        </Text>
                                    ) :
                                    null
                            }
                            <Text style={styles.time}>{dateUtils.getDate(this.props.createdAt)}</Text>
                        </View>
                        {
                            this.props.title ?
                                <Text
                                    style={styles.title}
                                    numberOfLines={2}
                                    ellipsizeMode='tail'
                                    selectable={true}
                                >
                                    {textUtils.capitalize(this.props.title)}
                                </Text> :
                                null
                        }
                        {
                            text ?
                                <View>
                                    <Autolink
                                        style={styles.text}
                                        selectable={true}
                                        text={text}
                                    />
                                    {
                                        isLongText ?
                                            <Text style={styles.more}>Читать далее</Text> :
                                            null
                                    }
                                </View> :
                                null
                        }
                    </View>
                </TouchableWithoutFeedback>
                {
                    this.props.previewSlider[0] && this.props.previewSlider[0].height / this.props.previewSlider[0].width > 1 ?
                        <Image
                            style={{ flex: 1 }}
                            height={Dimensions.get('window').width * (this.props.previewSlider[0].height / this.props.previewSlider[0].width)}
                            source={{ uri: this.props.previewSlider[0].src }}
                        /> :
                        <Slider
                            buttonWrapperStyle={{ left: 5, paddingRight: 40 }}
                            height={220}
                            items={this.props.previewSlider}
                        />
                }
                <View style={[globalStyles.helpers.centrify, styles.controls]}>
                    <View style={[globalStyles.helpers.centrify, styles.comment]}>
                        <Icon
                            name='comment'
                            style={styles.comment__icon}
                            size={23}
                            color='#000'
                            onPress={() => this.navigate({ isScrollToComment: true })}
                        />
                        {
                            this.props.commentsCountPublished ?
                                <Text style={styles.comment__count}>{this.props.commentsCountPublished}</Text> :
                                null
                        }
                    </View>
                    <Like
                        addLike={() => this.props.addLike(this.props._id)}
                        removeLike={() => this.props.removeLike(this.props._id)}
                        isVoted={Boolean(this.props.user.dump) && this.props.likes.includes(this.props.user.dump.id)}
                        count={this.props.likes.length}
                    />
                </View>
            </View>
        );
    }

    navigate(params = {}) {
        const categoryId = this.props.categories && this.props.categories[0];

        if (categoryId) {
            params.category = this.props.categoriesData[categoryId];
        }

        this.props.navigate('News', Object.assign({ id: this.props._id }, params));
    }
}
