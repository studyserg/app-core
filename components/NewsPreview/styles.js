import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        elevation: 2,
        paddingBottom: 5,
        marginBottom: 20
    },

    wrapper: {
        paddingHorizontal: 5,
        paddingVertical: 15
    },

    info: {
        justifyContent: 'flex-start',
        marginBottom: 5
    },

    category: {
        fontSize: 12,
        fontWeight: '800',

        color: globalStyles.constants.APP_MAIN_COLOR,

        marginRight: 5
    },

    time: {
        fontSize: 10,

        color: '#999'
    },

    title: {
        fontSize: 26,
        fontWeight: '700',

        color: '#000',

        marginBottom: 10,
    },

    text: {
        fontFamily: globalStyles.constants.FONT_REGULAR,

        color: '#000',
        marginBottom: 5,
        fontSize: 16
    },

    more: {
        fontFamily: globalStyles.constants.FONT_REGULAR,
        fontSize: 13,

        color: globalStyles.constants.APP_MAIN_COLOR,

        marginTop: 3
    },

    image: {
        height: 220
    },

    controls: {
        margin: 10
    },

    image_type_vertical: {
        maxHeight: 150
    },

    comment__icon: {
        paddingRight: 5,
        paddingVertical: 5
    },

    comment__count: {
        fontSize: 11
    }
});
