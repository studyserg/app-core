import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import DialogAndroid from 'react-native-dialogs';
import imagePickerUtils from './utils';

export default class ImagePicker extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        const data = [
            { key: 0, label: 'Сделать фото' },
            { key: 1, label: 'Выбрать фото из галереи' }
        ];

        return (
            <Icon
                name='camera'
                size={40}
                color='#000'
                onPress={() => this.pickImage()}
            />
        );
    }

    pickImage() {
        const dialog = new DialogAndroid();

        dialog.set({
            title: 'Добавить изображение',
            items: [
                'Сделать фото',
                'Выбрать фото из галереи'
            ],
            itemsCallbackSingleChoice: id => {
                switch(id) {
                    case 0:
                        imagePickerUtils.pickCameraImage(this.props.addImage);
                        break;
                    case 1:
                        imagePickerUtils.pickGalleryImage(this.props.addImage);
                        break;
                }
            }
        });
        dialog.show();
    }
}
