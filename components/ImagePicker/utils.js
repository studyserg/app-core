import ImagePicker from 'react-native-image-crop-picker';

function pickGalleryImage(onSelect) {
    ImagePicker
        .openPicker({
            width: 600,
            height: 400,
            cropping: true,
            mediaType: 'photo'
        })
        .then(image => onSelect(image))
        .catch(() => {});
}

function pickCameraImage(onSelect) {
    ImagePicker
        .openCamera({
            width: 600,
            height: 400,
            cropping: true,
            mediaType: 'photo'
        })
        .then(image => onSelect(image))
        .catch(() => {});
}

export default {
    pickGalleryImage,
    pickCameraImage
};
