import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import ModalSelector from 'react-native-modal-selector';
import imagePickerUtils from './utils';

export default class ImagePicker extends Component {
    shouldComponentUpdate() {
        return false;
    }

    render() {
        const data = [
            { key: 0, label: 'Сделать фото' },
            { key: 1, label: 'Выбрать фото из галереи' }
        ];

        return (
            <ModalSelector
                ref={ref => this._modal = ref}
                animationType='fade'
                data={data}
                cancelStyle={{ display: 'none' }}
                onChange={option => {
                    setTimeout(() => {
                        switch (option.key) {
                            case 0:
                                imagePickerUtils.pickCameraImage(this.props.addImage);
                                break;
                            case 1:
                                imagePickerUtils.pickGalleryImage(this.props.addImage);
                                break;
                        }
                    }, 800);
                }}
            >
                <Icon
                    name='camera'
                    size={40}
                    color='#000'
                />
            </ModalSelector>
        );
    }
}
