import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    input: {
        fontFamily: globalStyles.constants.FONT_REGULAR,
        textAlignVertical: 'top',

        paddingHorizontal: 10
    }
});
