import React, { PureComponent } from 'react';
import {
    Dimensions,
    Keyboard,
    LayoutAnimation,
    Platform,
    StatusBar,
    TextInput,
    View
} from 'react-native';
import styles from './styles';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : StatusBar.currentHeight;
const HEADER_HEIGHT = Platform.OS === 'ios' ? 64 : 56;

const window = Dimensions.get('window');

export default class FlexInput extends PureComponent {
    state = {
        height: 100
    };

    componentWillMount() {
        this.keyboardWillShowSub = Keyboard.addListener(
            'keyboardDidShow',
            this.onKeyboardChange.bind(this)
        );
        this.keyboardWillHideSub = Keyboard.addListener(
            'keyboardDidHide',
            this.onKeyboardChange.bind(this)
        );
    }

    componentWillUnmount() {
        this.keyboardWillShowSub.remove();
        this.keyboardWillHideSub.remove();
    }

    render() {
        return (
            <View style={{ height: this.state.height }}>
                <TextInput
                    style={[styles.input, { height: this.state.height - this.props.footerHeight }]}
                    autoFocus={true}
                    multiline={true}
                    placeholder={this.props.placeholder}
                    value={this.props.height}
                    onChangeText={text => this.props.onTextChange(text)}
                    underlineColorAndroid='transparent'
                />
                {this.props.footer}
            </View>
        )
    }

    onKeyboardChange(event) {
        const height = HEADER_HEIGHT +
            STATUSBAR_HEIGHT +
            (this.props.paddingHeight || 0);

        if (!event) {
            this.setState({ height: window.height - height });
            return;
        }

        const {
            duration,
            easing,
            endCoordinates
        } = event;

        if (duration && easing) {
            LayoutAnimation.configureNext({
                duration: duration,
                update: {
                    duration: duration,
                    type: LayoutAnimation.Types[easing] || 'keyboard',
                },
            });
        }

        this.setState({
            height: window.height - endCoordinates.height - height
        });
    }
};
