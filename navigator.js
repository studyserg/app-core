import { StackNavigator } from 'react-navigation';
import HomeScreen from './screens/Home';
import CategoryScreen from './screens/Category';
import NewsScreen from './screens/News';
import MessageScreen from './screens/Message';
import VacancyMessageScreen from './screens/VacancyMessage';
import FeedbackScreen from './screens/Feedback';
import PopularScreen from './screens/Popular';

export default new StackNavigator({
    Home: { screen: HomeScreen },
    News: { screen: NewsScreen },
    Message: { screen: MessageScreen },
    VacancyMessage: { screen: VacancyMessageScreen },
    Category: { screen: CategoryScreen },
    Feedback: { screen: FeedbackScreen },
    Popular: { screen: PopularScreen }
});
