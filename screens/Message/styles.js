import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    success: {
        flex: 1,
        alignItems: 'center',

        paddingTop: 50
    },

    success__text: {
        fontSize: 20,

        color: globalStyles.constants.APP_MAIN_COLOR
    },

    image__list: {
        flexDirection: 'row'
    },

    image: {
        margin: 5,

        elevation: 2
    },

    footer: {
        height: 60
    },

    footerInner: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

        paddingHorizontal: 10,

        backgroundColor: '#dfdfdf'
    }
});
