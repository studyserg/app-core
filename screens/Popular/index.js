import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
    FlatList,
    RefreshControl,
    View
} from 'react-native';
import { navigate } from '../../actions/nav';
import {
    addLike,
    extendNewsList,
    getNewsList,
    refreshNewsList,
    removeLike
} from '../../actions/news';
import globalStyles from '../../styles/index';
import Navicon from '../../components/Navicon/index';
import NewsPreview from '../../components/NewsPreview/index';

@connect(
    state => ({
        categoriesData: state.categories.data,
        isFetching: state.news.isFetching,
        news: state.news.items,
        user: state.user
    }),
    dispatch => ({
        extendNewsList: () => dispatch(extendNewsList(undefined, 'commentsCountPublished')),
        getNewsList: () => dispatch(getNewsList(undefined, 'commentsCountPublished')),
        refreshNewsList: () => dispatch(refreshNewsList(undefined, 'commentsCountPublished')),
        addLike: id => dispatch(addLike(id)),
        removeLike: id => dispatch(removeLike(id)),
        navigate: (route, params) => dispatch(navigate(route, params))
    })
)
export default class CategoryScreen extends PureComponent {
    static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.state.params.name,
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Navicon
            navigation={navigation}
            getDrawer={screenProps.getDrawer}
        />
    });

    componentDidMount() {
        this.props.getNewsList(this.props.navigation.state.params._id);
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.props.news}
                    keyExtractor={item => item._id}
                    renderItem={info => {
                        return (
                            <NewsPreview
                                {...info.item}
                                categoriesData={this.props.categoriesData}
                                user={this.props.user}
                                navigate={this.props.navigate}
                                addLike={this.props.addLike}
                                removeLike={this.props.removeLike}
                            />
                        );
                    }}

                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.isFetching}
                            onRefresh={() => this.props.refreshNewsList(this.props.navigation.state.params._id)}
                        />
                    }

                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={10}
                    initialNumToRender={10}
                    onEndReachedThreshold={500}
                    renderToHardwareTextureAndroid={true}
                    removeClippedSubviews={true}
                    onEndReached={() => this.props.extendNewsList(this.props.navigation.state.params._id)}
                    enableEmptySections={true}
                />
            </View>
        );
    }
}
