import { StyleSheet } from 'react-native';
import globalStyles from '../../styles/index';

export default StyleSheet.create({
    vacancy: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        marginVertical: 10,

        elevation: 10,

        backgroundColor: '#fff'
    },

    vacancy__text: {
        fontFamily: globalStyles.constants.FONT_BOLD,
        fontSize: 15,

        color: globalStyles.constants.APP_MAIN_COLOR,
        textAlign: 'center'
    }
});
