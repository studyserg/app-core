import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
    addLike,
    extendNewsList,
    getNewsList,
    refreshNewsList,
    removeLike
} from '../../actions/news';
import { getCategoryList } from '../../actions/categories';
import { navigate } from '../../actions/nav';
import Navicon from '../../components/Navicon/index';
import ParallaxScrollView from '../../components/ParallaxScrollView/index';
import globalStyles from '../../styles/index';

@connect(
    state => ({
        categoriesData: state.categories.data,
        isFetching: state.news.isFetching,
        news: state.news.items,
        user: state.user,
        district: state.district.data
    }),
    dispatch => ({
        getCategoryList: () => dispatch(getCategoryList()),
        addLike: id => dispatch(addLike(id)),
        extendNewsList: () => dispatch(extendNewsList()),
        getNewsList: () => dispatch(getNewsList()),
        navigate: (route, params) => dispatch(navigate(route, params)),
        refreshNewsList: () => dispatch(refreshNewsList()),
        removeLike: id => dispatch(removeLike(id))
    })
)
export default class HomeScreen extends PureComponent {
    static navigationOptions = navigation => ({
        title: 'Лента',
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Navicon
            getDrawer={navigation.screenProps.getDrawer}
        />
    });

    componentDidMount() {
        this.props
            .getCategoryList()
            .then(() => this.props.getNewsList());
    }

    render() {
        return (
            <ParallaxScrollView
                categoriesData={this.props.categoriesData}
                district={this.props.district}
                navigate={this.props.navigate}
                news={this.props.news}
                refreshing={this.props.isFetching}
                user={this.props.user}

                addLike={this.props.addLike}
                loadMore={() => this.props.extendNewsList()}
                onRefresh={() => this.props.refreshNewsList()}
                removeLike={this.props.removeLike}
            />
        );
    }
}
