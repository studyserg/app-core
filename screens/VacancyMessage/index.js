import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
    Image,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons'
import API from '../../utils/api';
import ImagePicker from '../../components/ImagePicker';
import FlexInput from '../../components/FlexInput/index';
import Text from '../../components/Text/index';
import Back from '../../containers/Back/index';
import globalStyles from '../../styles/index';
import styles from './styles';

const PLACEHOLDER_TEXT = 'Предложите свою вакансию. Помимо описания вакансии не забудьте указать рабочий ' +
    'телефон и, по возможности, адрес места работы.';

@connect(
    state => ({
        categoriesData: state.categories.data
    })
)
export default class MessageScreen extends PureComponent {
    static navigationOptions = {
        title: 'Добавить вакансию',
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Back />
    };

    state = {
        text: '',
        images: [],
        submited: false
    };

    render() {
        return (
            this.state.submited ?
                <View style={styles.success}>
                    <Text style={styles.success__text}>
                        Ваша новость
                    </Text>
                    <Text style={styles.success__text}>
                        отправлена на модерацию
                    </Text>
                </View> :
                <FlexInput
                    placeholder={PLACEHOLDER_TEXT}
                    footerHeight={this.state.images.length ? 130 : 60}
                    text={this.state.text}
                    onTextChange={text => this.setState({ text })}
                    footer={this._renderFooter()}
                />
        );
    }

    _renderFooter() {
        return (
            <View>
                {
                    this.state.images.length ?
                        <View style={styles.image__list}>
                            {
                                this.state.images.map((image, index) => {
                                    return (
                                        <View key={index} style={styles.image}>
                                            <Image
                                                style={{height: 60, width: 60}}
                                                source={{uri: image}}
                                            />
                                        </View>
                                    );
                                })
                            }
                        </View> :
                        null
                }
                <View style={styles.footer}>
                    <View style={styles.footerInner}>
                        <ImagePicker
                            addImage={image => this.setState({ images: this.state.images.concat(image.path) })}
                        />
                        <Icon
                            name='sc-telegram'
                            size={40}
                            color={this.state.text ? '#000' : '#b7bbbd'}
                            onPress={() => this.save()}
                        />
                    </View>
                </View>
            </View>
        );
    }

    save() {
        const text = this.state.text;

        if (!text) return;

        const images = this.state.images;
        const vacancyCategoryId = Object
            .keys(this.props.categoriesData)
            .find(id => this.props.categoriesData[id].name === 'Вакансии');

        Promise
            .all(images.map(path => API.imageUpload(path)))
            .then(names => {
                return Promise
                    .all(images.map(path => this.getImageSize(path)))
                    .then(sizes => {
                        return sizes.map((size, index) => {
                            return {
                                src: `http://185.41.161.107:8080/uploads/${names[index]}`,
                                width: size.width,
                                height: size.height
                            };
                        });
                    })
                    .then(slider => {
                        const data = {
                            categories: [vacancyCategoryId],
                            text: text,
                            slider: slider,
                            previewSlider: slider
                        };

                        API
                            .newsSave(data)
                            .then(() => this.setState({ submited: true }));
                    });
            });
    }

    getImageSize(uri) {
        return new Promise(resolve => {
            Image.getSize(uri, (width, height) => {
                resolve({ width, height });
            });
        });
    }
}
