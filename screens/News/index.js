import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Bubbles } from 'react-native-loader';
import { addCommentItem, getCommentList } from '../../actions/comments';
import { setUsername } from '../../actions/user';
import {
    addLike,
    fetchCurrentNews,
    removeLike
} from '../../actions/news';
import Back from '../../containers/Back/index';
import News from '../../components/News/index';
import globalStyles from '../../styles/index';
import styles from './styles';

@connect(
    state => ({
        categoriesData: state.categories.data,
        comments: state.comments.items,
        current: state.news.current,
        nav: state.nav,
        user: state.user
    }),
    dispatch => ({
        addLike: id => dispatch(addLike(id)),
        addCommentItem: (newsId, text) => dispatch(addCommentItem(newsId, text)),
        fetchCurrentNews: id => dispatch(fetchCurrentNews(id)),
        getCommentList: newsId => dispatch(getCommentList(newsId)),
        removeLike: id => dispatch(removeLike(id)),
        setUsername: name => dispatch(setUsername(name))
    })
)
export default class NewsScreen extends PureComponent {
    static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.state.params.category ? navigation.state.params.category.name : 'Лента',
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Back />
    });

    componentDidMount() {
        const id = this.props.navigation.state.params.id;

        this.props.fetchCurrentNews(id);
        this.props.getCommentList(id);
    }

    render() {
        return (
            this.props.current ?
                <News
                    {...this.props.current}

                    categoriesData={this.props.categoriesData}
                    comments={this.props.comments}
                    user={this.props.user}
                    isScrollToComment={this.props.navigation.state.params.isScrollToComment}

                    addCommentItem={this.props.addCommentItem}
                    addLike={this.props.addLike}
                    removeLike={this.props.removeLike}
                    setUsername={this.props.setUsername}
                /> :
                <View style={styles.spinner}>
                    <Bubbles
                        size={5}
                        color={globalStyles.constants.APP_MAIN_COLOR}
                    />
                </View>
        );
    }
}
