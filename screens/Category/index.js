import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
    FlatList,
    RefreshControl,
    View
} from 'react-native';
import { navigate } from '../../actions/nav';
import {
    addLike,
    extendNewsList,
    getNewsList,
    refreshNewsList,
    removeLike
} from '../../actions/news';
import globalStyles from '../../styles/index';
import Navicon from '../../components/Navicon/index';
import NewsPreview from '../../components/NewsPreview/index';
import Text from '../../components/Text/index';
import { Touchable } from '../../utils/components';
import styles from './styles';

const VACANCY_GATEGORY_NAME = 'Вакансии';

@connect(
    state => ({
        categoriesData: state.categories.data,
        isFetching: state.news.isFetching,
        news: state.news.items,
        user: state.user
    }),
    dispatch => ({
        addLike: id => dispatch(addLike(id)),
        extendNewsList: category => dispatch(extendNewsList(category)),
        getNewsList: category => dispatch(getNewsList(category)),
        navigate: (route, params) => dispatch(navigate(route, params)),
        refreshNewsList: category => dispatch(refreshNewsList(category)),
        removeLike: id => dispatch(removeLike(id))
    })
)
export default class CategoryScreen extends PureComponent {
    static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.state.params.name,
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Navicon
            navigation={navigation}
            getDrawer={screenProps.getDrawer}
        />
    });

    componentDidMount() {
        this.props.getNewsList(this.props.navigation.state.params._id);
    }

    render() {
        return (
            <View>
                {
                    this.props.navigation.state.params.name === VACANCY_GATEGORY_NAME ?
                        <Touchable onPress={() => this.props.navigate('VacancyMessage')}>
                            <View style={styles.vacancy}>
                                <Text style={styles.vacancy__text}>
                                    Бесплатно добавить вакансию
                                </Text>
                            </View>
                        </Touchable> :
                        null
                }
                <FlatList
                    data={this.props.news}
                    keyExtractor={item => item._id}
                    renderItem={info => {
                        return (
                            <NewsPreview
                                {...info.item}
                                categoriesData={this.props.categoriesData}
                                user={this.props.user}
                                navigate={this.props.navigate}
                                addLike={this.props.addLike}
                                removeLike={this.props.removeLike}
                            />
                        );
                    }}

                    refreshControl={
                        <RefreshControl
                            refreshing={this.props.isFetching}
                            onRefresh={() => this.props.refreshNewsList(this.props.navigation.state.params._id)}
                        />
                    }

                    showsVerticalScrollIndicator={false}
                    scrollEventThrottle={10}
                    initialNumToRender={10}
                    onEndReachedThreshold={500}
                    renderToHardwareTextureAndroid={true}
                    removeClippedSubviews={true}
                    onEndReached={() => this.props.extendNewsList(this.props.navigation.state.params._id)}
                    enableEmptySections={true}
                />
            </View>
        );
    }
}
