import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons'
import Back from '../../containers/Back/index';
import Api from '../../utils/api';
import FlexInput from '../../components/FlexInput/index';
import globalStyles from '../../styles/index';
import styles from './styles';

export default class FeedbackScreen extends PureComponent {
    static navigationOptions = navigation => ({
        title: 'Написать нам',
        headerStyle: { backgroundColor: globalStyles.constants.APP_MAIN_COLOR },
        headerTintColor: globalStyles.constants.HEADER_TEXT_COLOR,
        headerTitleStyle: { fontFamily: globalStyles.constants.FONT_REGULAR, fontWeight: '300' },
        headerLeft: <Back />
    });

    state = {
        text: '',
        isSended: false
    };

    render() {
        return (
            this.state.isSended ?
                <View style={styles.success}>
                    <Text style={styles.success__text}>
                        Ваше сообщение отправлено. Спасибо за обратную связь!
                    </Text>
                </View> :
                <FlexInput
                    placeholder="Введите ваш вопрос или пожелание"
                    footerHeight={60}
                    text={this.state.text}
                    onTextChange={text => this.setState({ text })}
                    footer={
                        <View style={styles.footer}>
                            <View style={styles.footerInner}>
                                <Icon
                                    name='sc-telegram'
                                    size={40}
                                    color={this.state.text ? '#000' : '#b7bbbd'}
                                    onPress={() => this.save()}
                                />
                            </View>
                        </View>
                    }
                />
        );
    }

    save() {
        if (!this.state.text) return;

        Api
            .feedbackCreate(this.state.text)
            .then(() => this.setState({ isSended: true }));
    }
}
