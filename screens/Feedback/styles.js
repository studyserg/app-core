import { StyleSheet } from 'react-native';
import globalStyles from '../../styles';

export default StyleSheet.create({
    success: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: 20,

        paddingTop: 50
    },

    success__text: {
        fontSize: 20,

        color: globalStyles.constants.APP_MAIN_COLOR
    },

    footer: {
        height: 60
    },

    footerInner: {
        flex: 1,
        flexDirection: 'row-reverse',
        alignItems: 'center',

        paddingHorizontal: 10,

        backgroundColor: '#dfdfdf'
    }
});
