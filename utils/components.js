import {
    Platform,
    TouchableOpacity,
    TouchableNativeFeedback,
} from 'react-native';

export const Touchable = Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
