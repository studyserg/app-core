const MS_MINUTE = 1000 * 60;
const MS_HOUR = MS_MINUTE * 60;
const MS_DAY = MS_HOUR * 24;

const getDate = timestamp => {
    const date = new Date(timestamp);
    const diff = Date.now() - date.getTime();

    if (diff < MS_HOUR) {
        const minutes = Math.floor(diff / MS_MINUTE);

        return diff > 0 && minutes ?
            `${minutes} мин. назад` :
            'только что';
    } else if (diff < MS_DAY) {
        return `${Math.floor(diff / MS_HOUR)} час. назад`;
    }

    let day = date.getDate();
    let month = date.getMonth() + 1;

    day < 10 && (day = `0${day}`);
    month < 10 && (month = `0${month}`);

    return `${day}.${month} ${date.getHours()}:${date.getMinutes()}`;
};

export default {
    getDate
};
