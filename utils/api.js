const DISTRICT_ID = '59b61d65b19e225186b37d85';

class API {
    constructor() {
        this.API_PATH = 'http://185.41.161.107:8080/api';
    }

    newsDetail(id) {
        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}/news/${id}`).then(res => res.json());
    }

    newsList(limit, skip, category, sort) {
        const query = `published=1&limit=${limit}&skip=${skip}${category ? '&category=' + category : ''}${sort ? '&sort=' + sort : ''}`;

        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}/news?${query}`)
            .then(res => res.json());
    }

    newsSave(data) {
        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}/news`, {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify(data)
        });
    }

    addLike(id, vkId) {
        return fetch(`${this.API_PATH}/news/like/${id}/add`, {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({ vkId })
        });
    }

    removeLike(id, vkId) {
        return fetch(`${this.API_PATH}/news/like/${id}/remove`, {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({ vkId })
        });
    }

    categoriesList() {
        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}/categories`)
            .then(response => response.json());
    }

    imageUpload(uri) {
        const body = new FormData();
        const name = `myImage-${Date.now()}.jpg`;
        const image = {
            uri: uri,
            type: 'image/jpg',
            name
        };

        body.append('image', image);

        return fetch(`${this.API_PATH}/image/upload/`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body
        }).then(() => name);
    }

    commentList(newsId, limit, skip) {
        const query = `newsId=${newsId}&limit=${limit}&skip=${skip}`;

        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}/comments?${query}`)
            .then(res => res.json());
    }

    commentSave(newsId, text, authorId, authorName) {
        return fetch(
            `${this.API_PATH}/districts/${DISTRICT_ID}/comments`, {
                method: 'POST',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({ newsId, text, authorId, authorName })
            })
            .then(response => response.json());
    }

    feedbackCreate(text) {
        return fetch(
            `${this.API_PATH}/districts/${DISTRICT_ID}/feedback`, {
                method: 'POST',
                headers: { 'Content-type': 'application/json' },
                body: JSON.stringify({ text })
            })
            .then(response => response.json());
    }

    districtGet() {
        return fetch(`${this.API_PATH}/districts/${DISTRICT_ID}`)
            .then(response => response.json());
    }
}

export default new API();
