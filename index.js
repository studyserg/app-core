import React from 'react';
import { Provider } from 'react-redux';
import { AppRegistry } from 'react-native';
import configureStore from './configureStore';
import App from './containers/App';

export default (name) => {
    const store = configureStore();

    const Districts = () => (
        <Provider store={store}>
            <App />
        </Provider>
    );

    AppRegistry.registerComponent(name, () => Districts);
}
